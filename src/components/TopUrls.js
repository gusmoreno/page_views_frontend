import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
   
class TopUrls extends Component {

    constructor(props) {
      super(props);
      this.state = {
        top_urls: {},
        active_date: ""
      };
      this.loadTopUrls = this.loadTopUrls.bind(this);
    }
    
    async loadTopUrls() {
      let response = await fetch(`http://localhost:3000/top_urls`);
      const top_urls = await response.json();
      this.setState({ 
        top_urls: top_urls, 
        active_date: Object.keys(top_urls)[0]
      });
    }
    
    componentDidMount() {
      this.loadTopUrls();
    }
      
    render() { 
      return (
        <div>
          <h4 className="title">URL visits per day</h4>
          <Tabs>
            { Object.keys(this.state.top_urls).map(key => {
                return (
                  <Tab key={ key } eventKey={ key } title={ key.split('-').reverse().join('/') }>
                    <Table striped bordered size="sm">
                    <thead>
                      <tr>
                        <th>Visited URL</th>
                        <th># of visits</th>
                      </tr>
                    </thead>
                    <tbody>
                      { this.state.top_urls[key].map((access, index) => {
                          return (
                            <tr key={ index }>
                              <td>{ access.url }</td>
                              <td>{ access.visits }</td>
                            </tr>
                          )
                        }) 
                      }
                    </tbody>
                    </Table>
                  </Tab>
                )
              }) 
            }
          </Tabs>
        </div>
      );
    }
}

export default TopUrls;