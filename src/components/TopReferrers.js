import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
   
class TopReferrers extends Component {

    constructor(props) {
      super(props);
      this.state = {
        top_referrers: {},
        active_date: ""
      };
      this.loadTopReferrers = this.loadTopReferrers.bind(this);
    }
    
    async loadTopReferrers() {
      let response = await fetch(`http://localhost:3000/top_referrers`);
      const top_referrers = await response.json();
      this.setState({ 
        top_referrers: top_referrers, 
        active_date: Object.keys(top_referrers)[0]
      });
    }
    
    componentDidMount() {
      this.loadTopReferrers();
    }
      
    render() {
      return (
        <div>
          <h4 className="title">Top URL visits and their main referrers per day</h4>
          <Tabs>
            { Object.keys(this.state.top_referrers).map(key => {
                return (
                  <Tab key={ key } eventKey={ key } title={ key.split('-').reverse().join('/') }>
                    <Table striped bordered size="sm">
                    <thead>
                      <tr>
                        <th>Visited URL</th>
                        <th># of visits</th>
                        <th>Reference URL</th>
                        <th># of references</th>
                      </tr>
                    </thead>
                    <tbody>
                      { this.state.top_referrers[key].map((access, a_index) => {
                          return access.referrers.map((referrer, r_index) => {
                            return (
                              <tr key={a_index.toString() + r_index.toString()}>
                                <td>{ access.url }</td>
                                <td>{ access.visits }</td>
                                <td>{ referrer.url }</td>
                                <td>{ referrer.visits }</td>
                              </tr>  
                            )
                          })
                        }) 
                      }
                    </tbody>
                    </Table>
                  </Tab>
                )
              }) 
            }
          </Tabs>
        </div>
      );
    }
}

export default TopReferrers;