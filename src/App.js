import React from 'react';
import './App.css';
import Header from './components/Header';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import TopUrls from './components/TopUrls';
import TopReferrers from './components/TopReferrers';
import Page404 from './components/Page404';

function App() {
  return (
    <div className="App">
      <Header />
      <div className="content"> 
        <BrowserRouter>
          <Switch>
              <Route path="/top_urls" component={TopUrls} />
              <Route path="/top_referrers" component={TopReferrers} />
              <Route path='*' component={Page404} />
          </Switch>
        </ BrowserRouter> 
      </div>
    </div>
  );
}

export default App;
