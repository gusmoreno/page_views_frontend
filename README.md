Page Views's test frontend

This is a frontend application bootstrapped with [Create React App](https://github.com/facebook/create-react-app), that consumes the [Page Views API](https://gitlab.com/gusmoreno/page_views_api)

# Getting started

## Instalation

`yarn install`

## Running the app

This app will run on port 3001 and consume the api on port 3000 per default. Simply run:

`yarn start`