import React, { Component } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
   
class Header extends Component {
    render() {
    return (
        <div>
            <Navbar bg="light">
                <Navbar.Brand>
                    Page Views App
                </Navbar.Brand>
                <Nav className="ml-auto">
                    <Nav.Link href="/top_urls">Top Urls</Nav.Link>
                    <Nav.Link href="/top_referrers">Top Referrers</Nav.Link>
                </Nav>
            </Navbar>
        </div>
    );
    }
}

export default Header;