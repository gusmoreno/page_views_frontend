import React, { Component } from 'react';
   
class Page404 extends Component {
    render() {
    return (
        <div className="helper-text">
            Click in one of the navbar links to check page views' information
        </div>
    );
    }
}

export default Page404;